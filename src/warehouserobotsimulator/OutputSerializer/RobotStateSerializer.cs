﻿using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.OutputSerializer
{
    public interface IRobotStateSerializer
    {
        string Serialize(RobotState robotState);
    }

    public class RobotStateSerializer : IRobotStateSerializer
    {
        private readonly IOrientationSerializer _orientationSerializer;
        private readonly IVectorSerializer _vectorSerializer;

        public RobotStateSerializer(IOrientationSerializer orientationSerializer, IVectorSerializer vectorSerializer)
        {
            _orientationSerializer = orientationSerializer;
            _vectorSerializer = vectorSerializer;
        }

        public string Serialize(RobotState robotState)
        {
            return $"{_vectorSerializer.Serialize(robotState.Position)} {_orientationSerializer.Serialize(robotState.Orientation)}";
        }
    }
}
