﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using warehouserobotsimulator.InputParser.Parsers;
using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.InputParser
{
    public interface IInputParser
    {
        Task<SimulationScenario> Parse(StreamReader sr);
    }

    public class InputParser : IInputParser
    {
        private readonly IActionsStringParser _actionsStringParser;
        private readonly IRobotStartStateStringParser _robotStartStateStringParser;
        private readonly ISimulationAreaEndpointStringParser _simulationAreaEndpointStringParser;

        public InputParser(IActionsStringParser actionsStringParser,
            IRobotStartStateStringParser robotStartStateStringParser,
            ISimulationAreaEndpointStringParser simulationAreaEndpointStringParser)
        {
            _actionsStringParser = actionsStringParser;
            _robotStartStateStringParser = robotStartStateStringParser;
            _simulationAreaEndpointStringParser = simulationAreaEndpointStringParser;
        }

        public async Task<SimulationScenario> Parse(StreamReader sr)
        {
            var simulationScenarion = new SimulationScenario
            {
                SimulationAreaEndPoint = _simulationAreaEndpointStringParser.Parse(await sr.ReadLineAsync()),
                RobotScenarios = new List<RobotScenario>()
            };

            while (!sr.EndOfStream)
            {
                var robotStartState = _robotStartStateStringParser.Parse(await sr.ReadLineAsync());
                var actions = _actionsStringParser.Parse(await sr.ReadLineAsync());

                simulationScenarion.RobotScenarios.Add(new RobotScenario
                {
                    RobotInitialState = robotStartState,
                    SimulationActions = actions,
                });
            }

            return simulationScenarion;
        }
    }
}
