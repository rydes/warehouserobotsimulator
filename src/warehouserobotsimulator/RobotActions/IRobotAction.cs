﻿using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.RobotActions
{
    public interface IRobotAction
    {
        void Perform(RobotState robotState, ISimulationArea simulationArea);
    }
}