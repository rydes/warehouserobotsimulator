﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using warehouserobotsimulator.InputParser;
using warehouserobotsimulator.InputParser.Parsers;
using warehouserobotsimulator.OutputSerializer;
using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator
{
    class Program
    {
        private static IInputParser _inputParser;
        private static ISimulator _simulator;
        private static IRobotStateSerializer _outputSerializer;
        private static IRectangularSimulationArea _simulationArea;
        private static string _simulationInputPath;
        public static string TestFilePath = "sample_input.txt";

        public static void Main(string[] args)
        {
            SetupInputFilePath(args);

            SetupComponents();

            var simulationOutputs = PerformSimulation();

            WriteOutput(simulationOutputs);

            Console.WriteLine("Simulation over. Press enter to exit.");
            Console.ReadLine();
        }

        private static void SetupInputFilePath(string[] args)
        {
            if (args.Length == 1)
            {
                var inputPath = args[0];
             
                if (!Path.IsPathRooted(inputPath))
                {
                    string programPath = new Uri(typeof(Program).GetTypeInfo().Assembly.CodeBase).LocalPath;
                    var programDirectory = Path.GetDirectoryName(programPath);
                    inputPath = Path.Combine(programDirectory, inputPath);
                }

                if (!File.Exists(inputPath))
                    throw new ArgumentException($"Input file doesn't exsists. Path: {inputPath}");

                _simulationInputPath = inputPath;

                Console.WriteLine($"Running simulation for input path: {inputPath}");
            }
            else
            {
                Console.WriteLine("Input path not provided. Running with sample data.");
                _simulationInputPath = TestFilePath;
            }
        }

        private static List<RobotState> PerformSimulation()
        {
            var simulationOutputs = new List<RobotState>();

            using (StreamReader sr = File.OpenText(_simulationInputPath))
            {
                if(sr.EndOfStream)
                    throw new ApplicationException("Input file is empty");

                var simulationScenario = _inputParser.Parse(sr).Result;
                _simulationArea.Setup(simulationScenario.SimulationAreaEndPoint);

                foreach (var robotScenario in simulationScenario.RobotScenarios)
                {
                    var simulation = new Simulation.Simulation
                    {
                        RobotState = robotScenario.RobotInitialState,
                        RobotActions = robotScenario.SimulationActions,
                        SimulationArea = _simulationArea
                    };
                    var simulationResult = _simulator.Simulate(simulation);
                    simulationOutputs.Add(simulationResult);
                }
            }

            return simulationOutputs;
        }

        private static void WriteOutput(List<RobotState> simulationOutputs)
        {
            foreach (var simulationOutput in simulationOutputs)
            {
                Console.WriteLine(_outputSerializer.Serialize(simulationOutput));
            }
        }
        public static void SetupComponents()
        {
            SetupInputParser();
            SetupSimulator();
            SetupOutputSerializer();
            SetupSimulationArea();
        }

        private static void SetupSimulationArea()
        {
            _simulationArea = new RectangularSimulationArea();
        }

        private static void SetupOutputSerializer()
        {
            _outputSerializer = new RobotStateSerializer(new OrientationSerializer(), new VectorSerializer());
        }

        private static void SetupSimulator()
        {
            _simulator = new Simulator();
        }

        private static void SetupInputParser()
        {
            var vectorParser = new VectorParser();
            _inputParser = new InputParser.InputParser(new ActionsStringParser(),
                new RobotStartStateStringParser(new RobotOrientationParser(), vectorParser),
                new SimulationAreaEndpointStringParser(vectorParser));
        }
    }
}
