﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.InputParser.Parsers;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class vector_parser_tests
    {
        [Fact]
        public void can_parse_string_into_vector()
        {
            //arrange
            var parser = new VectorParser();
            var input = "12 3";
            var expectedVector = new Vector2D(12, 3);

            //act
            var vector = parser.Parse(input);

            //assert
            vector.Should().Be(expectedVector);
        }

        [Fact]
        public void should_throw_argument_exception_when_input_string_is_null()
        {
            //arrange
            var parser = new VectorParser();
            var input = (string) null;

            //act
            Action act = () => parser.Parse(input);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void should_throw_argument_exception_when_input_string_is_empty()
        {
            //arrange
            var parser = new VectorParser();
            var input = string.Empty;

            //act
            Action act = () => parser.Parse(input);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }


        [Fact]
        public void should_throw_argument_exception_when_input_string_format_is_invalid()
        {
            //arrange
            var parser = new VectorParser();
            var input = "1 ";

            //act
            Action act = () => parser.Parse(input);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }
    }
}
