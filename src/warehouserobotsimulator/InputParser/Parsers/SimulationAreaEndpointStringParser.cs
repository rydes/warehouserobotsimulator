﻿using System;
using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.InputParser.Parsers
{
    public interface ISimulationAreaEndpointStringParser
    {
        /// <summary>
        /// Parses coordinates of area end point. Coordinates should be separated by space character.
        /// </summary>
        /// <param name="endpointString"></param>
        Vector2D Parse(string endpointString);
    }

    public class SimulationAreaEndpointStringParser : ISimulationAreaEndpointStringParser
    {
        private readonly IVectorParser _vectorParser;
      
        public SimulationAreaEndpointStringParser(IVectorParser vectorParser)
        {
            _vectorParser = vectorParser;
        }

        /// <summary>
        /// Parses coordinates of area end point. Coordinates should be separated by space character.
        /// </summary>
        /// <param name="endpointString"></param>
        public Vector2D Parse(string endpointString)
        {
            return _vectorParser.Parse(endpointString);
        }
    }
}