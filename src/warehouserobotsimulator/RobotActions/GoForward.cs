﻿using System;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.RobotActions
{
    public class GoForward : IRobotAction
    {
        public void Perform(RobotState robotState, ISimulationArea simulationArea)
        {
            ValidateRobotState(robotState);

            var newPosition = GetNewRobotPosition(robotState);

            if (simulationArea.CanMoveToPosition(newPosition))
                robotState.Position = newPosition;
        }

        private Vector2D GetNewRobotPosition(RobotState robotState)
        {
            var newPosition = new Vector2D(robotState.Position.X + robotState.Orientation.X,
                robotState.Position.Y + robotState.Orientation.Y);
            return newPosition;
        }

        private void ValidateRobotState(RobotState robotState)
        {
            RobotShouldHaveNonZeroOrientation(robotState.Orientation);
        }

        private void RobotShouldHaveNonZeroOrientation(Vector2D orientation)
        {
            if (orientation.Y == 0 && orientation.X == 0)
                throw new ArgumentException($"Robot should have non zero orientation. Robot orientation: {orientation}");
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(GoForward))
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return typeof(GoForward).GetHashCode();
        }

    }
}