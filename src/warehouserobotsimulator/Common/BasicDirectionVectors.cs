﻿namespace warehouserobotsimulator.Common
{
    public static class BasicDirectionVectors
    {
        public static Vector2D NorthOrientedVector = new Vector2D(0, 1);
        public static Vector2D WestOrientedVector = new Vector2D(-1, 0);
        public static Vector2D SouthOrientedVector = new Vector2D(0, -1);
        public static Vector2D EastOrientedVector = new Vector2D(1, 0);
    }
}
