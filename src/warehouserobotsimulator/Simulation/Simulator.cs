﻿using System.Collections.Generic;
using warehouserobotsimulator.RobotActions;

namespace warehouserobotsimulator.Simulation
{
    public interface ISimulator
    {
        RobotState Simulate(Simulation simulation);
    }

    public class Simulator : ISimulator
    {
        public RobotState Simulate(Simulation simulation)
        {
            foreach (var action in simulation.RobotActions)
            {
                action.Perform(simulation.RobotState, simulation.SimulationArea);
            }

            return simulation.RobotState;
        }
    }

    public class Simulation
    {
        public RobotState RobotState { get; set; }
        public List<IRobotAction> RobotActions { get; set; }
        public ISimulationArea SimulationArea { get; set; }
    }
}
