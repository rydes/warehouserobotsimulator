﻿using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.OutputSerializer;
using Xunit;

namespace warehouserobotsimulator.tests.SerializerTests
{
    public class vector_serializer_tests
    {
        [Fact]
        public void can_serialize_vector()
        {
            //arrange
            var vector = new Vector2D(1, 3);
            var serializer = new VectorSerializer();
            var expectedOutput = "1 3";

            //act
            var serializedVector = serializer.Serialize(vector);

            //assert
            serializedVector.Should().Be(expectedOutput);
        }
    }
}
