﻿using System;
using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.Simulation
{
    public interface ISimulationArea
    {
        bool CanMoveToPosition(Vector2D position);
    }

    public interface IRectangularSimulationArea : ISimulationArea
    {
        ISimulationArea Setup(Vector2D areaEndPoint);
    }

    public class RectangularSimulationArea : IRectangularSimulationArea
    {
        private  Vector2D _areaStartPoint;
        private  Vector2D _areaEndPoint;
        private bool _isSetup = false;

       
        public ISimulationArea Setup(Vector2D areaEndPoint)
        {
            _areaStartPoint = new Vector2D(0, 0);
            _areaEndPoint = areaEndPoint;

            IsValidAreaEndpoint(areaEndPoint);

            _isSetup = true;
            return this;
        }

        private void IsValidAreaEndpoint(Vector2D areaEndPoint)
        {
            if (areaEndPoint.X <= _areaStartPoint.X || _areaEndPoint.Y <= _areaStartPoint.Y)
                throw new ArgumentException($"Area endpoint coordinates have to be higher that startpoint. Area endpoint: {areaEndPoint}");
        }

        public bool CanMoveToPosition(Vector2D position)
        {
            if(!_isSetup)
                throw new InvalidOperationException("Simulation needs to be setup first");

            if (_areaStartPoint.X <= position.X &&
                _areaStartPoint.Y <= position.Y &&
                _areaEndPoint.X >= position.X &&
                _areaEndPoint.Y >= position.Y)
                return true;

            return false;
        }
    }
}