﻿using warehouserobotsimulator.Simulation;
using System;
using System.Text.RegularExpressions;

namespace warehouserobotsimulator.InputParser.Parsers
{
    public interface IRobotStartStateStringParser
    {
        RobotState Parse(string startStateString);
    }

    public class RobotStartStateStringParser : IRobotStartStateStringParser
    {
        private readonly IRobotOrientationParser _robotOrientationParser;
        private readonly IVectorParser _vectorParser;
        private const string InputParsingRegexPattern = @"(?<Position>\d*\s\d*)\s(?<Orientation>[A-Z])";
        public RobotStartStateStringParser(IRobotOrientationParser robotOrientationParser, IVectorParser vectorParser)
        {
            _robotOrientationParser = robotOrientationParser;
            _vectorParser = vectorParser;
        }

        /// <summary>
        /// Parses start position string. String should contain robot start coordinates and orientation.
        /// </summary>
        /// <param name="startStateString"></param>
        /// <returns></returns>
        public RobotState Parse(string startStateString)
        {
            ValidateInputString(startStateString);

            var match = TryGetMatch(startStateString);

            var position = _vectorParser.Parse(match.Groups["Position"].Value);
            var orientation = _robotOrientationParser.ParseOrientation(match.Groups["Orientation"].Value);

            return new RobotState
            {
                Position = position,
                Orientation = orientation
            };
        }

        private void ValidateInputString(string startStateString)
        {
            if (string.IsNullOrEmpty(startStateString))
                throw new ArgumentException($"Invalid startStateString: {startStateString}");
        }

        private Match TryGetMatch(string startStateString)
        {
            var match = Regex.Match(startStateString, InputParsingRegexPattern, RegexOptions.ExplicitCapture);
            if (match.Groups.Count != 3)
                throw new ArgumentException($"Invalid startStateString: {startStateString}");
            return match;
        }
    }
}