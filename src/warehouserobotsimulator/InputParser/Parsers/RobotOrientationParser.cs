﻿using System;
using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.InputParser.Parsers
{
    public interface IRobotOrientationParser
    {
        /// <summary>
        /// Parses robot orientation. Orientation should be represented with single capital letter.
        /// </summary>
        /// <param name="orientation"></param>
        /// <returns></returns>
        Vector2D ParseOrientation(string orientation);
    }

    public class RobotOrientationParser : IRobotOrientationParser
    {
        /// <summary>
        /// Parses robot orientation. Orientation should be represented with single capital letter.
        /// </summary>
        /// <param name="orientation"></param>
        /// <returns></returns>
        public Vector2D ParseOrientation(string orientation)
        {
            switch (orientation)
            {
                case "N":
                    return BasicDirectionVectors.NorthOrientedVector;
                case "S":
                    return BasicDirectionVectors.SouthOrientedVector;
                case "E":
                    return BasicDirectionVectors.EastOrientedVector;
                case "W":
                    return BasicDirectionVectors.WestOrientedVector;
                default:
                    throw new ArgumentException($"Unknown robot orientation: {orientation}");
            }
        }
    }
}