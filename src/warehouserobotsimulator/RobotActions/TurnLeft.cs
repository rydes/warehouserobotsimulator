﻿using System;
using warehouserobotsimulator.Simulation;
using Vector2D = warehouserobotsimulator.Common.Vector2D;

namespace warehouserobotsimulator.RobotActions
{
    public class TurnLeft : IRobotAction
    {
        public void Perform(RobotState robotState, ISimulationArea simulationArea)
        {
            ValidateRobotState(robotState);

            robotState.Orientation =  Rotate90DegLeft(robotState.Orientation);
        }

        private Vector2D Rotate90DegLeft(Vector2D robotOrientationVector)
        {
           return new Vector2D(-robotOrientationVector.Y, robotOrientationVector.X);
        }

        private void ValidateRobotState(RobotState robotState)
        {
            RobotShouldHaveNonZeroOrientation(robotState.Orientation);
        }

        private void RobotShouldHaveNonZeroOrientation(Vector2D orientation)
        {
            if (orientation.Y == 0 && orientation.X == 0)
                throw new ArgumentException($"Robot should have non zero orientation. Robot orientation: {orientation}");
        }
        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(TurnLeft))
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return typeof(TurnLeft).GetHashCode();
        }

    }
}