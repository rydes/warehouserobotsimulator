﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.InputParser.Parsers;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class robot_orientation_parser_tests
    {
        [Fact]
        public void can_parse_orientations()
        {
            //arrange
            var parser = new RobotOrientationParser();

            var northVector = BasicDirectionVectors.NorthOrientedVector;
            var southVector = BasicDirectionVectors.SouthOrientedVector;
            var eastVector = BasicDirectionVectors.EastOrientedVector;
            var westVector = BasicDirectionVectors.WestOrientedVector;

            //act and assert
            parser.ParseOrientation("N").Should().Be(northVector);
            parser.ParseOrientation("S").Should().Be(southVector);
            parser.ParseOrientation("E").Should().Be(eastVector);
            parser.ParseOrientation("W").Should().Be(westVector);
        }

        [Fact]
        public void should_throw_agrument_exception_when_orientation_unknown()
        {
            //arrange
            var parser = new RobotOrientationParser();

            //act
            Action act = () => parser.ParseOrientation("NW");

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }
    }
}
