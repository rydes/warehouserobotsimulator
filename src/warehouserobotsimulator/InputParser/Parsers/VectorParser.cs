﻿using System;
using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.InputParser.Parsers
{
    public interface IVectorParser
    {
        /// <summary>
        /// Parses string into Vector2D. Input string should containt two numbers separed whit space.
        /// </summary>
        /// <param name="vectorString">String with vector coordinates separated with space</param>
        /// <returns></returns>
        Vector2D Parse(string vectorString);
    }

    public class VectorParser : IVectorParser
    {
        /// <summary>
        /// Parses string into Vector2D. Input string should containt two numbers separed whit space.
        /// </summary>
        /// <param name="vectorString">String with vector coordinates separated with space</param>
        /// <returns></returns>
        public Vector2D Parse(string vectorString)
        {
            ValidateInputString(vectorString);

            var coordinates = TrySeparateCoordinates(vectorString);

            var vector = TryParseVector(coordinates);

            return vector;
        }

        private Vector2D TryParseVector(string[] coordinates)
        {
            return new Vector2D(coordinates[0].ToInt(), coordinates[1].ToInt());
        }

        private static string[] TrySeparateCoordinates(string vectorString)
        {
            var coordinates = vectorString.Trim(' ').Split(" ");

            if (coordinates.Length != 2)
                throw new ArgumentException($"Invalid input string: {vectorString}");

            return coordinates;
        }
        private static void ValidateInputString(string vectorString)
        {
            if (string.IsNullOrEmpty(vectorString))
                throw new ArgumentException($"Invalid input string: {vectorString}");
        }
    }
}
