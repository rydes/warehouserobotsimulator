﻿using Moq;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.tests.RobotActionsTests
{
    public class TurnActionFixture
    {
        private ISimulationArea _simulationArea;

        public TurnActionFixture()
        {
            SetupSimulationAreaMock();
        }

        private void SetupSimulationAreaMock()
        {
            var mock = new Mock<ISimulationArea>();
            mock.Setup(x => x.CanMoveToPosition(It.IsAny<Vector2D>())).Returns(true);
            _simulationArea = mock.Object;
        }

        public ISimulationArea GetMockedSimulationArea() => _simulationArea;
        public RobotState GetRobotStateWithNotSetupOrientation() => new RobotState { Orientation = default(Vector2D), Position = new Vector2D(1, 1) };
        public RobotState GetRobotWithOrientation(Vector2D orientation) => new RobotState { Orientation = orientation, Position = new Vector2D(1, 1) };
    }
}