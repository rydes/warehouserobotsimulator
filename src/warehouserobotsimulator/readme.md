This piece of software is a warehouse robot simulator. Detailed operation manual can be provided on request.
On default this app runs with sample input text file. You can also provide path to your input file as an argument.

All rights reserved,
Pawel Sadowski