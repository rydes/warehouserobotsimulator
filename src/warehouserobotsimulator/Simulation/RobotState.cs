﻿using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.Simulation
{
    public class RobotState
    {
        public Vector2D Position { get; set; }
        public Vector2D Orientation { get; set; }
    }
}