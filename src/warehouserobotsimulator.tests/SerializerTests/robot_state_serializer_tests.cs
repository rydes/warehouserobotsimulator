﻿using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.OutputSerializer;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.SerializerTests
{
    public class robot_state_serializer_tests
    {
        [Fact]
        public void can_serialize_robot_state()
        {
            //arrange
            var robotState = new RobotState
            {
                Orientation = BasicDirectionVectors.NorthOrientedVector,
                Position = new Vector2D(3, 2)
            };
            var expectedOutput = "3 2 N";
            var robotStateSerializer = new RobotStateSerializer(new OrientationSerializer(), new VectorSerializer());

            //act
            var serializedRobotState = robotStateSerializer.Serialize(robotState);

            //assert
            serializedRobotState.Should().Be(expectedOutput);
        }
    }
}
