﻿using System;
using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.OutputSerializer
{
    public interface IOrientationSerializer
    {
        string Serialize(Vector2D orientationVector);
    }

    public class OrientationSerializer : IOrientationSerializer
    {
        private const string NorthVectorRepresentation = "N";
        private const string SouthVectorRepresentation = "S";
        private const string WestVectorRepresentation = "W";
        private const string EastVectorRepresentation = "E";

        public string Serialize(Vector2D orientationVector)
        {
            if (orientationVector == BasicDirectionVectors.NorthOrientedVector)
                return NorthVectorRepresentation;
            if (orientationVector == BasicDirectionVectors.SouthOrientedVector)
                return SouthVectorRepresentation;
            if (orientationVector == BasicDirectionVectors.WestOrientedVector)
                return WestVectorRepresentation;
            if (orientationVector == BasicDirectionVectors.EastOrientedVector)
                return EastVectorRepresentation;

            throw new ArgumentException($"Unknow vector: {orientationVector}");
        }
    }
}