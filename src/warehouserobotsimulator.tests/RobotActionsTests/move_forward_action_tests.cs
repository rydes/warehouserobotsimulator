﻿using System;
using FluentAssertions;
using Moq;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.RobotActions;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.RobotActionsTests
{
    public class move_forward_action_tests
    {
        [Fact]
        public void should_thorw_if_robot_orientation_vector_is_not_set()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotStateWithNotSetupOrientation();

            //act
            Action moveNotOrientedRobot = () => goForwardAction.Perform(robotState, fixture.GetInfiniteSimulationArea());

            //assert
            moveNotOrientedRobot.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void can_go_north()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotWithOrientation(BasicDirectionVectors.NorthOrientedVector);
            var initialRobotPostion = robotState.Position;
            var initialRobotOrientation = robotState.Orientation;

            //act
            goForwardAction.Perform(robotState, fixture.GetInfiniteSimulationArea());

            //assert
            robotState.Position.Should().Be(new Vector2D(initialRobotPostion.X, initialRobotPostion.Y+1));
            robotState.Orientation.Should().Be(initialRobotOrientation);
        }

        [Fact]
        public void can_go_west()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotWithOrientation(BasicDirectionVectors.WestOrientedVector);
            var initialRobotPostion = robotState.Position;
            var initialRobotOrientation = robotState.Orientation;

            //act
            goForwardAction.Perform(robotState, fixture.GetInfiniteSimulationArea());

            //assert
            robotState.Position.Should().Be(new Vector2D(initialRobotPostion.X - 1, initialRobotPostion.Y));
            robotState.Orientation.Should().Be(initialRobotOrientation);
        }

        [Fact]
        public void can_go_south()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotWithOrientation(BasicDirectionVectors.SouthOrientedVector);
            var initialRobotPostion = robotState.Position;
            var initialRobotOrientation = robotState.Orientation;

            //act
            goForwardAction.Perform(robotState, fixture.GetInfiniteSimulationArea());

            //assert
            robotState.Position.Should().Be(new Vector2D(initialRobotPostion.X, initialRobotPostion.Y-1));
            robotState.Orientation.Should().Be(initialRobotOrientation);
        }

        [Fact]
        public void can_go_east()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotWithOrientation(BasicDirectionVectors.EastOrientedVector);
            var initialRobotPostion = robotState.Position;
            var initialRobotOrientation = robotState.Orientation;

            //act
            goForwardAction.Perform(robotState, fixture.GetInfiniteSimulationArea());

            //assert
            robotState.Position.Should().Be(new Vector2D(initialRobotPostion.X +1, initialRobotPostion.Y));
            robotState.Orientation.Should().Be(initialRobotOrientation);
        }

        [Fact]
        public void wont_move_outside_simulation_area()
        {
            //arrange
            var fixture = new GoForwardFixture();
            var goForwardAction = new GoForward();
            var robotState = fixture.GetRobotWithOrientation(BasicDirectionVectors.NorthOrientedVector);
            var initialRobotPostion = robotState.Position;
            var initialRobotOrientation = robotState.Orientation;

            //act
            goForwardAction.Perform(robotState, fixture.GetInfiniteSmallSimulationArea());

            //assert
            robotState.Position.Should().Be(initialRobotPostion);
            robotState.Orientation.Should().Be(initialRobotOrientation);
        }

        class GoForwardFixture
        {
            private ISimulationArea _inifiteSimulationArea;
            private ISimulationArea _infiniteSmallSimulationArea;

            public GoForwardFixture()
            {
                SetupInfiniteSimulationArea();
                SetupIniniteSmallSimulationArea();
            }


            private void SetupInfiniteSimulationArea()
            {
                var mock = new Mock<ISimulationArea>();
                mock.Setup(x => x.CanMoveToPosition(It.IsAny<Vector2D>())).Returns(true);
                _inifiteSimulationArea = mock.Object;
            }
            private void SetupIniniteSmallSimulationArea()
            {
                var mock = new Mock<ISimulationArea>();
                mock.Setup(x => x.CanMoveToPosition(It.IsAny<Vector2D>())).Returns(false);
                _infiniteSmallSimulationArea = mock.Object;
            }

            public ISimulationArea GetInfiniteSimulationArea() => _inifiteSimulationArea;
            public ISimulationArea GetInfiniteSmallSimulationArea() => _infiniteSmallSimulationArea;
            public RobotState GetRobotStateWithNotSetupOrientation() => new RobotState { Orientation = default(Vector2D), Position = new Vector2D(3, 3) };
            public RobotState GetRobotWithOrientation(Vector2D orientation) => new RobotState { Orientation = orientation, Position = new Vector2D(3, 3) };

        }
    }
}
