﻿using System;

namespace warehouserobotsimulator.RobotActions
{
    public class RobotActionFactory
    {
        public IRobotAction Create(char actionCode)
        {
            switch (actionCode)
            {
                case 'L':
                    return new TurnLeft();
                case 'R':
                    return new TurnRight();
                case 'F':
                    return new GoForward();
                default:
                    throw new ArgumentException($"Unknow robot action: {actionCode}");
            }
        }
    }
}