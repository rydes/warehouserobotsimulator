﻿using System;
using System.Collections.Generic;
using FluentAssertions;
using warehouserobotsimulator.InputParser.Parsers;
using warehouserobotsimulator.RobotActions;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class action_string_parser_tests
    {
        [Fact]
        public void can_parse_go_left_action()
        {
            //arrange
            var actionStringParser = new ActionsStringParser();
            var inputString = "LFR";
            var expectedActions = new List<IRobotAction>
            {
                new TurnLeft(),
                new GoForward(),
                new TurnRight()
            };

            //act
            var parsedActions = actionStringParser.Parse(inputString);
            parsedActions.Should().Equal(expectedActions);
        }

        [Fact]
        public void should_throw_argument_null_exception__when_input_is_empty()
        {
            //arrange
            var actionStringParser = new ActionsStringParser();
            var inputString = String.Empty;

            //act
            Action act = () => actionStringParser.Parse(inputString);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void should_throw_argument_null_exception_when_input_is_null()
        {
            //arrange
            var actionStringParser = new ActionsStringParser();
            var inputString = (string) null;

            //act
            Action act = () => actionStringParser.Parse(inputString);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }
    }
}
