﻿using FluentAssertions;
using System.Collections.Generic;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.RobotActions;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.SimulatorTests
{
    public class simulation_tests
    {
        [Fact]
        public void can_get_final_robot_position1()
        {
            //arrange
            var fixture = new SimulatorFixture();
            var expectedRobotState = new RobotState
            {
                Orientation = BasicDirectionVectors.NorthOrientedVector,
                Position = new Vector2D(1, 3)
            };

            var simulator = new Simulator();

            //act
            var finalRobotState =
                simulator.Simulate(fixture.SimulationData);

            //assert
            finalRobotState.Should().BeEquivalentTo(expectedRobotState);
        }

        public class SimulatorFixture
        {
            public Simulation.Simulation SimulationData = new Simulation.Simulation
            {
                RobotState = new RobotState
                {
                    Orientation = BasicDirectionVectors.NorthOrientedVector,
                    Position = new Vector2D(1, 2),
                },
                RobotActions = new List<IRobotAction>
                {
                    new TurnLeft(),
                    new GoForward(),
                    new TurnLeft(),
                    new GoForward(),
                    new TurnLeft(),
                    new GoForward(),
                    new TurnLeft(),
                    new GoForward(),
                    new GoForward(),
                },
                SimulationArea =
                    new RectangularSimulationArea().Setup(new Vector2D(5, 5))
            };
        }
    }
}
