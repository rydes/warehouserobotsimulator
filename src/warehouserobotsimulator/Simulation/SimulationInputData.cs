﻿using System.Collections.Generic;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.RobotActions;

namespace warehouserobotsimulator.Simulation
{
    public class SimulationScenario
    {
        public Vector2D SimulationAreaEndPoint { get; set; }
        public List<RobotScenario> RobotScenarios { get; set; }
    }

    public class RobotScenario
    {
        public RobotState RobotInitialState { get; set; }
        public List<IRobotAction> SimulationActions { get; set; }
    }
}