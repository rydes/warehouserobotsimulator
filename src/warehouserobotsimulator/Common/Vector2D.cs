﻿namespace warehouserobotsimulator.Common
{
    public struct Vector2D
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2D(int x, int y)
        {
            X = x;
            Y = y;
        }
        public static bool operator ==(Vector2D v1, Vector2D v2)
        {
            return v1.X == v2.X && v1.Y == v2.Y;
        }

        public static bool operator !=(Vector2D v1, Vector2D v2)
        {
            return v1.X != v2.X || v1.Y != v2.Y;
        }
    }
}