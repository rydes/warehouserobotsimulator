﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.SimulatorTests
{
    public class square_area_simulator_tests
    {
        [Fact]
        public void CanMoveToPosition_returns_true_if_point_is_in_area()
        {
            //arrange
            var area = new RectangularSimulationArea();
            area.Setup(new Vector2D(5,5));
            var pointInsideArea = new Vector2D(1, 1);

            //act
            var isInArea = area.CanMoveToPosition(pointInsideArea);

            //assert
            isInArea.Should().Be(true);
        }

        [Fact]
        public void CanMoveToPosition_returns_false_if_point_is_outside_area()
        {
            //arrange
            var area = new RectangularSimulationArea();
            area.Setup(new Vector2D(5,5));
            var pointOutsideArea = new Vector2D(6, 1);

            //act
            var isInArea = area.CanMoveToPosition(pointOutsideArea);

            //assert
            isInArea.Should().Be(false);
        }

        [Fact]
        public void CanMoveToPosition_throws_InvalidOperationException_if_simulation_hasnt_been_setup()
        {
            //arrange
            var area = new RectangularSimulationArea();
            var point = new Vector2D(6, 1);

            //act
            Action executeOnNotSetupArea = ()=> area.CanMoveToPosition(point);

            //assert
            executeOnNotSetupArea.Should().ThrowExactly<InvalidOperationException>();
        }

        [Fact]
        public void Setup_throws_when_trying_to_setup_with_inncorect_end_point()
        {
            //arrange
            var area = new RectangularSimulationArea();
            var invalidEndpoint = new Vector2D(6, 0);

            //act
            Action setupWithIncorrectEndPoint = () => area.Setup(invalidEndpoint);

            //assert
            setupWithIncorrectEndPoint.Should().ThrowExactly<ArgumentException>();
        }
    }
}
