﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.OutputSerializer;
using Xunit;

namespace warehouserobotsimulator.tests.SerializerTests
{
    public class orientation_serializer_tests
    {
        [Fact]
        public void should_throw_when_orientation_isnt_basic_direction()
        {
            //arrange
            var unknowOrientation = new Vector2D(3, 1);
            var orientationSerializer = new OrientationSerializer();

            //act
            Action serializeUnknowOrientation = () => orientationSerializer.Serialize(unknowOrientation);

            //assert
            serializeUnknowOrientation.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void can_serialize_north_orientation()
        {
            //arrange
            var orientationSerializer = new OrientationSerializer();

            //act
            var serializedOrientation =  orientationSerializer.Serialize(BasicDirectionVectors.NorthOrientedVector);

            //assert
            serializedOrientation.Should().Be("N");
        }

        [Fact]
        public void can_serialize_south_orientation()
        {
            //arrange
            var orientationSerializer = new OrientationSerializer();

            //act
            var serializedOrientation = orientationSerializer.Serialize(BasicDirectionVectors.SouthOrientedVector);

            //assert
            serializedOrientation.Should().Be("S");
        }

        [Fact]
        public void can_serialize_west_orientation()
        {
            //arrange
            var orientationSerializer = new OrientationSerializer();

            //act
            var serializedOrientation = orientationSerializer.Serialize(BasicDirectionVectors.WestOrientedVector);

            //assert
            serializedOrientation.Should().Be("W");
        }

        [Fact]
        public void can_serialize_east_orientation()
        {
            //arrange
            var orientationSerializer = new OrientationSerializer();

            //act
            var serializedOrientation = orientationSerializer.Serialize(BasicDirectionVectors.EastOrientedVector);

            //assert
            serializedOrientation.Should().Be("E");
        }
    }
}
