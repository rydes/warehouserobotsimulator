using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.InputParser.Parsers;
using warehouserobotsimulator.RobotActions;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class InputParserTests
    {
        const string TestFilePath = @".\InputParserTests\input_parser_example_data.txt";

        [Fact]
        public async Task can_load_load_input_file_into_input_data_structure()
        {
            //arrange
            var expectedOutput = new SimulationScenario
            {
                SimulationAreaEndPoint = new Vector2D(5, 5),
                RobotScenarios = new List<RobotScenario>
                {
                    new RobotScenario
                    {
                        RobotInitialState = new RobotState()
                        {
                            Orientation = BasicDirectionVectors.NorthOrientedVector,
                            Position = new Vector2D(1, 2),
                        },
                        SimulationActions = new List<IRobotAction>()
                        {
                            new TurnLeft(), new GoForward(),
                            new TurnLeft(), new GoForward(),
                            new TurnLeft(), new GoForward(),
                            new TurnLeft(), new GoForward(), new GoForward(),
                        }
                    }
                }
            };

            var inputParser = new InputParserFixture().GetSut();

            //act
            SimulationScenario parsedInput;

            using (StreamReader sr = File.OpenText(TestFilePath))
            {
                parsedInput = await inputParser.Parse(sr);
            }

            //assert
            parsedInput.RobotScenarios.Count.Should().Be(1);
            parsedInput.SimulationAreaEndPoint.Should().BeEquivalentTo(expectedOutput.SimulationAreaEndPoint);
            parsedInput.RobotScenarios.First().RobotInitialState.Should().BeEquivalentTo(expectedOutput.RobotScenarios[0].RobotInitialState);
            parsedInput.RobotScenarios.First().SimulationActions.Should().Equal(expectedOutput.RobotScenarios[0].SimulationActions);
        }

        public class InputParserFixture
        {
            public InputParser.InputParser GetSut()
            {
                return new InputParser.InputParser(new ActionsStringParser()
                    , new RobotStartStateStringParser(new RobotOrientationParser(), new VectorParser()),
                    new SimulationAreaEndpointStringParser(new VectorParser()));
            }
        }
    }
}