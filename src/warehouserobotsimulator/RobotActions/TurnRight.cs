﻿using System;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.Simulation;

namespace warehouserobotsimulator.RobotActions
{
    public class TurnRight : IRobotAction
    {
        public void Perform(RobotState robotState, ISimulationArea simulationArea)
        {
            ValidateRobotState(robotState);

            robotState.Orientation = Rotate90DegRight(robotState.Orientation);
        }

        private Vector2D Rotate90DegRight(Vector2D robotOrientationVector)
        {
            return new Vector2D(robotOrientationVector.Y, -robotOrientationVector.X);
        }

        private void ValidateRobotState(RobotState robotState)
        {
            RobotShouldHaveNonZeroOrientation(robotState.Orientation);
        }

        private void RobotShouldHaveNonZeroOrientation(Vector2D orientation)
        {
            if (orientation.Y == 0 && orientation.X == 0)
                throw new ArgumentException($"Robot should have non zero orientation. Robot orientation: {orientation}");
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(TurnRight))
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return typeof(TurnRight).GetHashCode();
        }
    }
}