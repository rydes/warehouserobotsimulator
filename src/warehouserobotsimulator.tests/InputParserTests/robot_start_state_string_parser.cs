﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.InputParser.Parsers;
using warehouserobotsimulator.Simulation;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class robot_start_state_string_parser
    {
        [Fact]
        public void can_parse_start_state_string()
        {
            //arrange
            var parser = new RobotStartStateStringParser(new RobotOrientationParser(), new VectorParser());
            var expectedStartState = new RobotState
            {
                Orientation = BasicDirectionVectors.NorthOrientedVector,
                Position = new Vector2D(2, 6)
            };
            var inputString = "2 6 N";

            //act
            var parsedStartState = parser.Parse(inputString);

            //assert
            parsedStartState.Should().BeEquivalentTo(expectedStartState);
        }

        [Fact]
        public void should_throw_argument_exception_when_input_is_null()
        {
            //arrange
            var parser = new RobotStartStateFixture().GetSut();
            var inputString = (string)null;

            //act
            Action act = () => parser.Parse(inputString);

            //assert
            act.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void should_throw_argument_exception_when_input_is_empty()
        {
            //arrange
            var parser = new RobotStartStateFixture().GetSut();
            var inputString = string.Empty;

            //act
            Action act = () => parser.Parse(inputString);

            //assert
            act.Should().Throw<ArgumentException>();
        }

        [Fact]
        public void should_throw_argument_exception_when_input_format_is_invalid()
        {
            //arrange
            var parser = new RobotStartStateFixture().GetSut();
            var inputString = "1 ";

            //act
            Action act = () => parser.Parse(inputString);

            //assert
            act.Should().Throw<ArgumentException>();
        }

    }

    public class RobotStartStateFixture
    {
        public RobotStartStateStringParser GetSut()
        {
            return new RobotStartStateStringParser(new RobotOrientationParser(), new VectorParser());
        }
    }
}
