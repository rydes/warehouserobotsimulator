﻿using warehouserobotsimulator.Common;

namespace warehouserobotsimulator.OutputSerializer
{
    public interface IVectorSerializer
    {
        string Serialize(Vector2D vector);
    }

    public class VectorSerializer : IVectorSerializer
    {
        public string Serialize(Vector2D vector)
        {
            return $"{vector.X} {vector.Y}";
        }
    }
}