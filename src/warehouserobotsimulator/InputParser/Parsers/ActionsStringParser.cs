﻿using System;
using System.Collections.Generic;
using warehouserobotsimulator.RobotActions;

namespace warehouserobotsimulator.InputParser.Parsers
{
    public interface IActionsStringParser
    {
        /// <summary>
        /// Parses actions string. Each action is represented with capital letter. Actions shouldn't be spearated with space or any character.
        /// </summary>
        /// <param name="actionsString"></param>
        /// <returns></returns>
        List<IRobotAction> Parse(string actionsString);
    }

    public class ActionsStringParser : IActionsStringParser
    {
        private readonly RobotActionFactory _robotActionFactory = new RobotActionFactory();
        /// <summary>
        /// Parses actions string. Each action is represented with capital letter. Actions shouldn't be spearated with space or any character.
        /// </summary>
        /// <param name="actionsString"></param>
        /// <returns></returns>
        public List<IRobotAction> Parse(string actionsString)
        {
            ValidateInput(actionsString);

            var actions = ParseActionsString(actionsString);

            return actions;
        }

        private List<IRobotAction> ParseActionsString(string actionsString)
        {
            var actions = new List<IRobotAction>();

            foreach (var actionCode in actionsString)
            {
                actions.Add(_robotActionFactory.Create(actionCode));
            }

            return actions;
        }

        private void ValidateInput(string actionsString)
        {
            if (string.IsNullOrEmpty(actionsString))
                throw new ArgumentException($"Invalid input string: {actionsString}");
        }
    }
}