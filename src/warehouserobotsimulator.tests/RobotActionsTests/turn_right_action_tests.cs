﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.RobotActions;
using warehouserobotsimulator.tests.RobotActionsTests;
using Xunit;

namespace warehouserobotsimulator.tests.ActionsTests
{
    public class turn_right_action_tests
    {
        [Fact]
        public void should_thorw_if_robot_orientation_vector_is_not_set()
        {
            //arrange
            var fixture = new TurnActionFixture();
            var turnRightAction = new TurnRight();
            var robotState = fixture.GetRobotStateWithNotSetupOrientation();

            //act
            Action rotateNotOrientedRobot = () => turnRightAction.Perform(robotState, fixture.GetMockedSimulationArea());

            //assert
            rotateNotOrientedRobot.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void can_rotate_360deg_right()
        {
            //arrange
            var fixture = new TurnActionFixture();
            var turnLeftAction = new TurnRight();
            var northOrientedRobot = fixture.GetRobotWithOrientation(BasicDirectionVectors.NorthOrientedVector);

            //act & assert
            turnLeftAction.Perform(northOrientedRobot, fixture.GetMockedSimulationArea());
            northOrientedRobot.Orientation.Should().Be(BasicDirectionVectors.EastOrientedVector);

            turnLeftAction.Perform(northOrientedRobot, fixture.GetMockedSimulationArea());
            northOrientedRobot.Orientation.Should().Be(BasicDirectionVectors.SouthOrientedVector);

            turnLeftAction.Perform(northOrientedRobot, fixture.GetMockedSimulationArea());
            northOrientedRobot.Orientation.Should().Be(BasicDirectionVectors.WestOrientedVector);

            turnLeftAction.Perform(northOrientedRobot, fixture.GetMockedSimulationArea());
            northOrientedRobot.Orientation.Should().Be(BasicDirectionVectors.NorthOrientedVector);
        }
    }
}
