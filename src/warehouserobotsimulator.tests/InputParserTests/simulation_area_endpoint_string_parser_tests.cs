﻿using System;
using FluentAssertions;
using warehouserobotsimulator.Common;
using warehouserobotsimulator.InputParser.Parsers;
using Xunit;

namespace warehouserobotsimulator.tests.InputParserTests
{
    public class simulation_area_endpoint_string_parser_tests
    {
        [Fact]
        public void can_parse_simulation_area_endpoint()
        {
            //arrange
            var parser = new SimulationAreaEndpointStringParserFixture().GetSut();
            var inputString = "23 31";
            var expectedEndpoint = new Vector2D(23, 31);

            //act
            var endPoint = parser.Parse(inputString);

            //assert
            endPoint.Should().Be(expectedEndpoint);
        }

        [Fact]
        public void should_throw_when_input_string_is_empty()
        {
            //arrange
            var parser = new SimulationAreaEndpointStringParserFixture().GetSut();
            var inputString = string.Empty;

            //act
            Action act = ()=> parser.Parse(inputString);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void should_throw_when_input_string_is_null()
        {
            //arrange
            var parser = new SimulationAreaEndpointStringParserFixture().GetSut();
            var inputString = (string)null;

            //act
            Action act = ()=> parser.Parse(inputString);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }

        [Fact]
        public void should_throw_when_input_string_format_is_invalid()
        {
            //arrange
            var parser = new SimulationAreaEndpointStringParserFixture().GetSut();
            var inputString = "1 2 3";

            //act
            Action act = () => parser.Parse(inputString);

            //assert
            act.Should().ThrowExactly<ArgumentException>();
        }
    }

    public class SimulationAreaEndpointStringParserFixture
    {
        public SimulationAreaEndpointStringParser GetSut()
        {
            return new SimulationAreaEndpointStringParser(new VectorParser());
        }
    }
}
