﻿using System;

namespace warehouserobotsimulator.Common
{
    public static class StringExtensions
    {
        public static int ToInt(this string input)
        {
            return Int32.Parse(input);
        }
    }
}